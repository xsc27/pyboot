#!/usr/bin/env sh

# Requirements:
#  * Bash
#  * git
#  * gcc
#  * libc headers
#  * make
# dnf install make gcc zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel openssl-devel tk-devel libffi-devel<Paste>
# bzip2-dev realine-dev openssl-dev libffi-dev
# Recommended:
#   * sqlite-dev
# aria2

# TODO: Create pre-flight checks
# TODO: Script to install system requirements (sudo)
# TODO: CLI paramaters to take commit hash and sha1 of file
# TODO: Support for shasum or sha1 commands

# TODO: Determine if already root
apk add --no-cache git bash gcc make libc-dev libffi-dev openssl-dev bzip2-dev zlib-dev readline-dev \
  sqlite-dev aria2

PYENV_INSTALLER_COMMIT=c5475539c464fa167406d7103ca92f09a5445ddb
PYENV_INSTALLER_HASH=178b6c0d0f669d481903dcc28aa1b9a53f5edf54
TMP_DIR=/tmp/pybootstrap

mkdir -p ${TMP_DIR}
cd ${TMP_DIR}



if [ ! -n "$(which pyenv)" ]; then
    which curl && DL_CMD="curl -O"
    which wget && DL_CMD=wget
    which sha1sum && SHA1_CMD="sha1sum -c"
    ${DL_CMD} https://github.com/pyenv/pyenv-installer/raw/${PYENV_INSTALLER_COMMIT}/bin/pyenv-installer
    SHA_CHECK="${PYENV_INSTALLER_HASH}  pyenv-installer"
    if [ ! -z "${SHA1_CMD}" ]; then
        printf "${SHA_CHECK}" | ${SHA1_CMD} || ERROR_MSG="Failed SHA1 check for pyenv-installer!"
    else
        printf "Could not verify sha.\n"
    fi

    if [ ! -z "${ERROR_MSG}" ]; then
        printf "${ERROR_MSG}\n"
        exit 1
    fi

    chmod +x ${TMP_DIR}/pyenv-installer
    ${TMP_DIR}/pyenv-installer
else
    cd "$(pyenv root)"
    git pull
    cd -
fi

mkdir -p "${HOME}/.local/bin"
ln -sv "${HOME}/.pyenv/bin/pyenv" "${HOME}/.local/bin/"

printf '# pyenv\nexport PATH="${HOME}/.local/bin:$PATH"\neval "$(pyenv init -)"\n' \
    >> "${HOME}/.profile"

export PATH="${HOME}/.local/bin:$PATH"
eval "$(pyenv init -)"

PY_VER=$(~/.pyenv/bin/pyenv install --list | grep '^ *\d.*[\.\d]\d$' | tail -n 1)
CONFIGURE_OPTS="--enable-optimizations" pyenv install --skip-existing ${PY_VER}

pyenv global ${PY_VER}

printf "$(python --version) has been set as the shell default.\n\n"

# PIPX_VENV="pipx.venv"

# python -m venv "${PIPX_VENV}"

# "${PIPX_VENV}/bin/pip" install pipx
# "${PIPX_VENV}/bin/pipx" install pipx
# rm -rf "${PIPX_VENV}"

pip install --user --upgrade pipx

pipx install pipenv

